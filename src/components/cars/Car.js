import React, {Fragment} from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import './Car.css';
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        button: {
            margin: theme.spacing(1),
        },
        input: {
            display: 'none',
        },
    }),
);

const Car = (props) => {
    const classes = useStyles();
    return (<Fragment>
        <ul>
            <li className="car"><span>{props.name}</span>:
                <span>{props.price}</span>
            </li>
        </ul>
        <Button variant="contained" color="primary" className={classes.button} onClick={props.incrementPrice}>
            Increament
        </Button>
        <Button variant="contained" color="primary" className={classes.button} onClick={props.decrementPrice}>
            Decreament
        </Button>
    </Fragment>)
}
export default Car