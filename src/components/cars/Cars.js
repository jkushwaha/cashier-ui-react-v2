import React, {Fragment} from 'react';
import {Consumer} from '../../context/MyContext';
import Car from '../../components/cars/Car'

const Cars = () => (
    <Consumer>
        { context => (
            <Fragment>
                <h4>Cars:</h4>
                {Object.keys(context.cars).map(carID => (
                    <Car
                        key={carID}
                        name={context.cars[carID].name}
                        price={context.cars[carID].price}
                        incrementPrice={() => context.incrementPrice(carID)}
                        decrementPrice={() => context.decrementPrice(carID)}
                    />
                ))}
            </Fragment>
        )}
    </Consumer>
);

export default Cars;