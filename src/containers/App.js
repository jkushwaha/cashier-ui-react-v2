import React, { Component } from 'react';
import Box from '@material-ui/core/Box';
import './App.css';
import ProductList from '../components/productlist/ProductList';
import MyProvider from '../provider/MyProvider';

class App extends Component {
  render() {
    return (
        <MyProvider>
      <div className="App">
        <Box m={1}>
        <div className="App-header">
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
          <ProductList/>
        </Box>
      </div>
        </MyProvider>

    );
  }
}

export default App;
