import React from 'react';

// this is the equivalent to the createStore method of Redux
// https://redux.js.org/api/createstore
const MyContext = React.createContext();
export const Provider = MyContext.Provider;
export const Consumer = MyContext.Consumer;
export default MyContext;