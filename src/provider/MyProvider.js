import React, {Component} from 'react';
import {Provider} from '../context/MyContext';
class MyProvider extends Component{
    state = {
        cars: {
            car001: { name: 'Honda', price: 100 },
            car002: { name: 'BMW', price: 150 },
            car003: { name: 'Mercedes', price: 200 }
        }
    };
    render(){
        return(
            <Provider
                value={{
                    cars: this.state.cars,
                    incrementPrice: selectedID => {
                        // console.log('incrementPrice selectedId::'+selectedID);
                        // const cars = Object.assign({}, this.state.cars);
                        const cars = {...this.state.cars};//alternative way.
                        cars[selectedID].price = cars[selectedID].price + 1;
                        this.setState({
                            cars
                        });
                    },
                    decrementPrice: selectedID => {
                        // console.log('decrementPrice selectedId::'+selectedID);
                        const cars = Object.assign({}, this.state.cars);
                        cars[selectedID].price = cars[selectedID].price - 1;
                        this.setState({
                            cars
                        });
                    }
                }}
            >
                {this.props.children}
            </Provider>
        );
    }
}

export default MyProvider;